# [NATS server](https://docs.nats.io/nats-server) apt packages

Requirements:

* systemd (Ubuntu >= 16.04)

### Add repo signing key to apt

```bash
sudo curl -sL -o /etc/apt/trusted.gpg.d/morph027-nats-server.asc https://packaging.gitlab.io/nats-server/gpg.key
```

### Add repo to apt

```bash
echo "deb https://packaging.gitlab.io/nats-server nats main" | sudo tee /etc/apt/sources.list.d/morph027-nats-server.list
```

### Install packages

```bash
sudo apt-get update
sudo apt-get install -y nats-server morph-keyring
```

### Configuration

Configuration file will be loaded from `/etc/nats/nats.conf`.

Create directory and configuration file:

```bash
sudo install -d -o nats -g nats /etc/nats
sudo -u nats echo "listen: 127.0.0.1:4222" > /etc/nats/nats.conf
```
