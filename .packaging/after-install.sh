getent passwd nats >/dev/null 2>&1 || adduser \
  --system \
  --shell /usr/sbin/nologin \
  --gecos 'High-Performance server for NATS, the cloud native messaging system.' \
  --group \
  --disabled-password \
  --no-create-home \
  nats
